const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({ path: './config.env' })
const app = require('./app')

mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log("Successfully connected mongo");
}).catch((error) => {
    console.log("HERE");
    console.log(error);
})
const port = 4000

app.listen(port,()=>{
    console.log(`app is running on port ${port}..`)
})