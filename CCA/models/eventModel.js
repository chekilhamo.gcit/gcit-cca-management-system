const mongoose= require("mongoose");
// const autoIncrement = require("mongoose-auto-increment");

// Initialize auto-increment
// autoIncrement.initialize(mongoose.connection);
const eventSchema  = new mongoose.Schema({
    eventName:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
   
    datetime:{
        type: String,
        // default:Date.now
    },
    status:{
        type:String,
        // enum: ['Pending','Approved','Rejected'],
        default: 'Pending'

    },
    remarks:{
        type:String,

    },
    supportingDocument:{
        type:String

    },
    userid:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User",
        required:true

    }],
    cca:{
        type:String,

    },
    datetimecca:{
        type: String,
        // default:Date.now()
    },
    ccaStatus:{
        type:String,
        default:'Pending'

    },
    ccaremarks:{
        type:String,

    },
    token:{
        type:String,
        default:''
    }

});
// eventSchema.plugin(autoIncrement.plugin, { model: 'Event', field: 'eventid' });

module.exports = mongoose.model("Event",eventSchema);