const mongoose= require("mongoose");

const ccaRequestSchema  = new mongoose.Schema({
    eventid:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:"Event"

    }],
    userid:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User"

    }],
    ccarolepointid:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:"CCARolepoint"

    }],
});

module.exports = mongoose.model("CCARequest",ccaRequestSchema);