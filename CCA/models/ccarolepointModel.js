const mongoose= require("mongoose");

const ccarolepointSchema  = new mongoose.Schema({
    role:{
        type:String,
        required:true,
        unique:true
    },
    components:{
        type:String,
    },
    points:{
        type:Number,
    },
});

module.exports = mongoose.model("CCARolepoint",ccarolepointSchema);