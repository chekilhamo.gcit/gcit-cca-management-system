const mongoose = require("mongoose")
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose. Schema({
    name: {type: String, 
    required:[ true,'Please enter your name!'],
    },
    userid: {
        type: String,
        required: [true, 'Please provide your userid'],
        unique: true, 
    },
    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true, 
        lowercase: true, 
        validate: [validator.isEmail ,'Please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    role: {
        type: String, 
        enum: ['hos','admin','student','staff'],
        default: 'student'
    }, 
    password:{
        type: String, 
        //required: [true,'Please provide a password!'],
        minlength: 8,
        //password wont be included when we get the users select: false,
    },  
    // passwordConfirm: {
    //     type: String,
    //     required:[true, 'Please confirm your password'],
    //     validate: function (el) {
    //         return el === this.password
    //     },
    //     message: 'Password are not same',
    // },
    year: {
        type: String, 
        //reuired:[true, 'Please provide your year'],
        // default: 'hos'
    }, 
    course: {
        type: String, 
        //reuired:[true, 'Please provide your course'],
        // default: 'hos'
    }, 
    designation: {
        type: String, 
        //reuired:[true, 'Please provide your year'],
        // default: 'hos'
    }, 
    joinedYear: {
        type: String, 
        //reuired:[true, 'Please provide your year'],
        // default: 'hos'
    },
    token:{
        type:String,
        default:''
    }, 
    active: {
        type: Boolean, 
        default: true, 
        select: false,
    }
    
})
// userSchema.pre('save',async function (next) {
//     if(!this.isModified('password')) return next()

//     this.password = await bcrypt.hash(this.password, 10)
//     this.passwordConfirm = undefined
//     next()
// })
// userSchema.pre('findOneAndUpdate',async function (next){
//     const update = this.getUpdate();
//     if(update.password !== ''&& 
//     update.password !== undefined &&
//     update.password == update.passwordConfirm){
    
//     this.getUpdate().password = await bcrypt.hash(update.password,12)

//     update.passwordConfirm = undefined
//     next()
//     }else
//     next()

// })
// userSchema.methods.correctPassword = async function (
//     candidatePassword,
//     userPassword,

// ) {
//     return await bcrypt.compare(candidatePassword, userPassword)
// }
// userSchema.methods.correctPassword = async function (candidatePassword) {
//     return await bcrypt.compare(candidatePassword, this.password);
//   };
const User = mongoose.model('User', userSchema)
module.exports = User


