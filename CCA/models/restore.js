const { exec } = require('child_process');
const path = require('path')

function restoreData() {
    const backupDir = path.join(__dirname, "backup"); // Specify the directory where the backup is saved
    const command = `mongorestore ${backupDir}`;

    exec(command, (error, stdout, stderr) => {
        if (error) {
        console.error(`Restore failed: ${error}`);
        } else {
        console.log(`Restore successful!`);
        }
    });
}
// restoreData();
