const User = require('../models/userModel');
const Event = require('../models/eventModel');
const { ConnectionStates } = require('mongoose');
const CCARequest = require('../models/ccaRequestModel')
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require("../config/config");

const securePassword = async (password) => {
  try {
    const passwordHash = await bcrypt.hash(password, 10);
    return passwordHash;
  } catch (error) {

    console.log(error.message);
  }
}
const loadStudentList = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    const users = await User.find({ role: "student" });

    res.render("hos/studentlist", { user: userData, users: users })

  } catch (error) {
    console.log(error.message)
  }

}
const loadHosDashboard = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    // const ccaData = await Event.find();

    const eventData = await Event.aggregate([
      {
        $lookup: {
          from: "users",
          localField: "userid",
          foreignField: "_id",
          as: "user"
        }
      },
      {
        $unwind: "$user"
      },
      {
        $project: {
          eventName: 1,
          description: 1,
          type: 1,
          datetime: 1,
          status: 1,
          remarks: 1,
          supportingDocument: 1,
          cca: 1,
          datetimecca: 1,
          ccaStatus: 1,
          "user.name": 1
        }
      }
    ]);
    // console.log(eventData)
    res.render("hos/hosDashboard", {
      user: userData,
      event: eventData
    });
  } catch (error) {
    console.log(error.message);
  }
};
const loadccarequestList = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    // const ccaData = await Event.find();

    const eventData = await Event.aggregate([
      {
        $lookup: {
          from: "users",
          localField: "userid",
          foreignField: "_id",
          as: "user"
        }
      },
      {
        $unwind: "$user"
      },
      {
        $project: {
          eventName: 1,
          description: 1,
          type: 1,
          datetime: 1,
          status: 1,
          remarks: 1,
          supportingDocument: 1,
          cca: 1,
          datetimecca: 1,
          ccaStatus: 1,
          "user.name": 1
        }
      }
    ]);
    // console.log(eventData)
    res.render("hos/ccarequestList", {
      user: userData,
      event: eventData
    });
  } catch (error) {
    console.log(error.message);
  }
};

const loadAbout = async (req, res) => {
  try {

    const userData = await User.findById({ _id: req.session.userid });
    console.log(userData)

    res.render("hos/about", { user: userData })

  } catch (error) {
    console.log(error.message)
  }
}

const loadsetting = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    res.render('hos/hosSetting', { admin: userData });

  } catch (error) {
    console.log(error.message)
  }

}
//update user profile

//update user profile
const updateProfile = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const photo = req.file.filename;

    if (photo) {
      userData.photo = photo;
      await userData.save();

    } const updatedUser = await User.findByIdAndUpdate(userId, {
      new: true,
      runValidators: true,
    });

    res.render('hos/hosSetting', {
      admin: updatedUser,
      message: "Successfully updated"
    });


  } catch (error) {
    return res.render('hos/hosSetting', {
      admin: userData,
      message: "Something went wrong"
    });

  }

}


//update user profile
const updatePassword = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const confirmPassword = req.body.confirmPassword;

    if (oldPassword && newPassword && confirmPassword) {
      const user = await User.findById(userId).select('+password');

      if (await bcrypt.compare(oldPassword, user.password)) {

        if (newPassword === confirmPassword) {
          const hashedPassword = await securePassword(newPassword);
          user.password = hashedPassword;
          await user.save();

          const updatedUser = await User.findByIdAndUpdate(userId, {
            new: true,
            runValidators: true,
          });

          return res.render('hos/hosSetting', {
            admin: updatedUser,
            message: "Password successfully updated",
          });
        } else {
          return res.render('hos/hosSetting', {
            admin: userData,
            message: "New password and confirm password do not match",
          });
        }
      } else {
        return res.render('hos/hosSetting', {
          admin: userData,
          message: "Old password is incorrect",
        });
      }
    } else {
      return res.render('hos/hosSetting', {
        admin: userData,
        message: "Please provide old password, new password, and confirm password",
      });
    }
  } catch (error) {
    return res.render('hos/hosSetting', {
      admin: userData,
      message: "Something went wrong",
    });
  }
};
const loadViewMoreEvent = async (req, res) => {
  try {
    const id = req.query.id;
    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.findById({ _id: id }).populate('userid');
    // res.local.data = { id: req.query.id}
    // const userName = eventData.userid[0].name;
    // console.log(userName)


    res.render("hos/hosViewMoreEvent", { user: userData, event: eventData })

  } catch (error) {
    console.log(error.message)
  }

}

const loadViewccarequest = async (req, res) => {
  try {
    const id = req.query.id;
    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.findById({ _id: id }).populate('userid');
    // res.local.data = { id: req.query.id}

    res.render("hos/viewccarequest", { user: userData, event: eventData })

  } catch (error) {
    console.log(error.message)
  }

}
const nodemailer = require('nodemailer');

// Create a transporter object with your email service provider's configuration
const transporter = nodemailer.createTransport({

  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: config.emailUser,
    pass: config.emailPassword
  }
});

const approveEvent = async (req, res) => {
  try {
    const id = req.query.id
    const userData = await User.findById({ _id: req.session.userid });

    const event = await Event.findById({ _id: id }).populate('userid');

    if (!event) {
      return res.status(404).send("Event not found");
    }

    event.status = "Approved";
    await event.save();

    const user = await User.findOne({ _id: event.userid });


    if (user) {
      // Send email to the event proposer
      const mailOptions = {
        from: config.emailUser,
        to: user.email,
        subject: 'Event Approved',
        text: `Dear ${event.userid[0].name},

        The "${event.eventName}" event that you have proposed on ${event.datetime} has been approved.
        
        Thank you.`,

      };

      await transporter.sendMail(mailOptions);
    }

    res.render("hos/hosViewMoreEvent", {
      user: userData,
      event: event,
      message: "Event Proposal Successfully Approved"
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

const rejectEvent = async (req, res) => {
  try {

    const id = req.query.id;
    const event = await Event.findById({ _id: id }).populate('userid');
    const userData = await User.findById({ _id: req.session.userid });

    if (!event) {
      return res.status(404).send("Event not found");
    }

    event.status = "Rejected";
    event.remarks = req.body.remarks;
    await event.save();

    const user = await User.findOne({ _id: event.userid });


    if (user) {
      // Send email to the event proposer
      const mailOptions = {
        from: config.emailUser,
        to: user.email,
        subject: 'Event Rejected',
        text: `Dear ${event.userid[0].name},
        
        The "${event.eventName}" event that you have proposed on ${event.datetime} has been rejected with the remarks "${event.remarks}".
        
        Thank you.`,
      };

      await transporter.sendMail(mailOptions);
    }

    res.render("hos/hosViewMoreEvent", {
      user: userData,
      event: event,
      message: " Event Proposal Successfully Rejected"
    });
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};
const loadStudentProfile = async (req, res) => {
  try {
    const id = req.query.id;

    const userData = await User.findById({ _id: req.session.userid });
    const userData1 = await User.findById({ _id: id });
    const ccaRequests = await CCARequest.aggregate([
      {
        $match: { userid: new mongoose.Types.ObjectId(id) },
      },
      {
        $lookup: {
          from: "events",
          localField: "eventid",
          foreignField: "_id",
          as: "event",
        },
      },
      {
        $unwind: {
          path: "$event",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "userid",
          foreignField: "_id",
          as: "user",
        },
      },
      {
        $unwind: {
          path: "$user",
        },
      },
      {
        $lookup: {
          from: "ccarolepoints",
          localField: "ccarolepointid",
          foreignField: "_id",
          as: "ccarolepoint",
        },
      },
      {
        $unwind: {
          path: "$ccarolepoint",
        },
      },
      {
        $project: {
          eventName: "$event.eventName",
          components: "$ccarolepoint.components",
          roleName: "$ccarolepoint.role",
          point: "$ccarolepoint.points",
          userId: "$user.userid",
          year: "$event.datetimecca"
        },
      }
    ]);
    var ParticipationPoint = 0
    var EnrichmentPoint = 0
    var AchievementPoint = 0
    var RepresentationPoint = 0
    var LeadershipPoint = 0
    var ServicePoint = 0





    for (let i = 0; i < ccaRequests.length; i++) {
      const element = ccaRequests[i];
      // console.log(`This is the element array: `+element.components)

      if (element.components === 'Participation') {
        ParticipationPoint = ParticipationPoint + element.point
        // ParticipationPoint = 23;
      }
      if (element.components === 'Enrichment') {
        EnrichmentPoint = EnrichmentPoint + element.point
        // LeadershipPoint = 19;


      }
      if (element.components === 'Achievement') {
        AchievementPoint = AchievementPoint + element.point
        // AchievementPoint = 15;
      }
      if (element.components === 'Representation') {
        RepresentationPoint = RepresentationPoint + element.point
      }
      if (element.components === 'Leadership') {
        LeadershipPoint = LeadershipPoint + element.point
        // CommunityServicePoint = 3;
      }
      if (element.components === 'Service') {
        ServicePoint = ServicePoint + element.point
      }
    }
    // console.log("Participation: "+ParticipationPoint,
    //             "Leadershipe: "+LeadershipPoint,
    //             "Achiement: "+AchievementPoint,
    //             "Enrichment: "+EnrichmentPoint,
    //             "CommunityService: "+CommunityServicePoint,
    //             "Service: "+ServicePoint)

    var participation;
    if (ParticipationPoint >= 25) {
      participation = 25;
    } else {
      participation = ParticipationPoint
    }
    var Total = participation + EnrichmentPoint + AchievementPoint + RepresentationPoint + LeadershipPoint + ServicePoint;
    var grade = "No Grade";

    if (Total >= 60 && ServicePoint >= 6 && (LeadershipPoint >= 12 || AchievementPoint >= 12)) {
      grade = "Platinum";
    } else if (Total >= 50 && ServicePoint >= 4 && (LeadershipPoint >= 8 || AchievementPoint >= 8)) {
      grade = "Gold";
    } else if (Total >= 35 && ServicePoint >= 2 && (LeadershipPoint >= 6 || AchievementPoint >= 6)) {
      grade = "Silver";
    } else if (Total >= 20 && ServicePoint >= 2) {
      grade = "Bronze";
    } else {
      grade = "No Grade";
    }

    // console.log(Total)   
    res.render('hos/hosViewStudentProfile', {
      user: userData,
      std: userData1,
      cca: ccaRequests,
      participation: participation,
      enrichment: EnrichmentPoint,
      achievement: AchievementPoint,
      representation: RepresentationPoint,
      leadership: LeadershipPoint,
      service: ServicePoint,
      total: Total,
      grade: grade
    });
  } catch (error) {
    console.log(error.message);
  }
};

const approveCCA = async (req, res) => {
  try {
    const id = req.query.id;
    // const event = await Event.findById(eventId);
    const userData = await User.findById({ _id: req.session.userid });
    const event = await Event.findById({ _id: id }).populate('userid');

    if (!event) {
      return res.status(404).send("Event not found");
    }

    event.ccaStatus = "Approved";
    await event.save();

    const user = await User.findOne({ _id: event.userid });


    if (user) {
      // Send email to the event proposer
      const mailOptions = {
        from: config.emailUser,
        to: user.email,
        subject: 'CAA Request Approved',
        text: `Dear ${event.userid[0].name},
        
        Your CCA request for the "${event.eventName}" event that you have proposed on ${event.datetime} has been Approved.
        
        Thank you.`,
      };

      await transporter.sendMail(mailOptions);
    }


    res.render("hos/viewccarequest", {
      user: userData,
      event: event,
      message: "CCA Proposal Successfully Approved"

    }

    );
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};
const rejectCCA = async (req, res) => {
  try {
    const id = req.query.id;
    const event = await Event.findById({ _id: id }).populate('userid');

    const userData = await User.findById({ _id: req.session.userid });


    if (!event) {
      return res.status(404).send("Event not found");
    }

    event.ccaStatus = "Rejected";
    event.ccaremarks = req.body.remarks;
    await event.save();

    const user = await User.findOne({ _id: event.userid });


    if (user) {
      // Send email to the event proposer
      const mailOptions = {
        from: config.emailUser,
        to: user.email,
        subject: 'CCA Request Rejected',
        text: `Dear ${event.userid[0].name},
        
        Your CCA request the "${event.eventName}" event that you have proposed on ${event.datetime} has been Rejected with the remarks "${event.ccaremarks}".
        
        Thank you.`,
      };

      await transporter.sendMail(mailOptions);
    }

    res.render("hos/viewccarequest", {
      user: userData,
      event: event,
      message: "CCA Proposal Successfully Rejected"

    });
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

module.exports = {
  loadHosDashboard,
  loadccarequestList,
  loadsetting,
  updateProfile,
  updatePassword,
  loadViewMoreEvent,
  approveEvent,
  rejectEvent,
  loadStudentProfile,
  approveCCA,
  rejectCCA,
  loadViewccarequest,
  loadStudentList,
  loadAbout

}