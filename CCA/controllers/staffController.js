const User = require('../models/userModel');
const Event = require('../models/eventModel');
const CCA = require('../models/ccaRequestModel');
const CCARolepoint = require('../models/ccarolepointModel');
const moment = require('moment-timezone');
const timezone = 'Asia/Thimphu';
const csv = require("csvtojson")
// const moment = require('moment-timezone');
const CCARequest = require('../models/ccaRequestModel')
const mongoose = require('mongoose');
const bcrypt = require('bcrypt')

const securePassword = async (password) => {
  try {
    const passwordHash = await bcrypt.hash(password, 10);
    return passwordHash;
  } catch (error) {

    console.log(error.message);

  }

}
const loadStaffDashboard = async (req, res) => {
  try {
    const id = req.session.userid;
    // console.log(id)

    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.find();
    // console.log(eventData)
    console.log()
    var arr = []
    for (let i = eventData.length - 1; i >= 0; i--) {
      const element = eventData[i]
      // console.log(id,element.userid)
      if (id == element.userid) {
        const userpost = eventData[i];
        arr.push(userpost)

        // console.log(userpost);
      }
    }
    // console.log(arr)
    res.render("staff/staffDashboard", { user: userData, event: arr })


  } catch (error) {
    console.log(error.message)
  }

}
const loadUserDetails = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    const users = await User.find({ role: "student" });

    res.render("staff/UserDetails", { user: userData, users: users, })

  } catch (error) {
    console.log(error.message)
  }

}

const loadAbout = async (req, res) => {
  try {
    console.log("i am here")

    const userData = await User.findById({ _id: req.session.userid });
    console.log(userData)

    res.render("staff/about", { staff: userData })

  } catch (error) {
    console.log(error.message)
  }
}
const loadViewUserDetails = async (req, res) => {
  try {
    const id = req.query.id;
    const userData = await User.findById({ _id: req.session.userid });
    const userStudent = await User.findById({ _id: id });
    const ccaRequests = await CCARequest.aggregate([
      {
        $match: { userid: new mongoose.Types.ObjectId(id) },
      },
      {
        $lookup: {
          from: "events",
          localField: "eventid",
          foreignField: "_id",
          as: "event",
        },
      },
      {
        $unwind: {
          path: "$event",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "userid",
          foreignField: "_id",
          as: "user",
        },
      },
      {
        $unwind: {
          path: "$user",
        },
      },
      {
        $lookup: {
          from: "ccarolepoints",
          localField: "ccarolepointid",
          foreignField: "_id",
          as: "ccarolepoint",
        },
      },
      {
        $unwind: {
          path: "$ccarolepoint",
        },
      },
      {
        $project: {
          eventName: "$event.eventName",
          components: "$ccarolepoint.components",
          roleName: "$ccarolepoint.role",
          point: "$ccarolepoint.points",
          userId: "$user.userid",
          year: "$event.datetimecca"
        },
      }
    ]);

    var ParticipationPoint = 0
    var EnrichmentPoint = 0
    var AchievementPoint = 0
    var RepresentationPoint = 0
    var LeadershipPoint = 0
    var ServicePoint = 0





    for (let i = 0; i < ccaRequests.length; i++) {
      const element = ccaRequests[i];
      // console.log(`This is the element array: `+element.components)

      if (element.components === 'Participation') {
        ParticipationPoint = ParticipationPoint + element.point
        // ParticipationPoint = 23;
      }
      if (element.components === 'Enrichment') {
        EnrichmentPoint = EnrichmentPoint + element.point
        // LeadershipPoint = 19;


      }
      if (element.components === 'Achievement') {
        AchievementPoint = AchievementPoint + element.point
        // AchievementPoint = 15;
      }
      if (element.components === 'Representation') {
        RepresentationPoint = RepresentationPoint + element.point
      }
      if (element.components === 'Leadership') {
        LeadershipPoint = LeadershipPoint + element.point
        // CommunityServicePoint = 3;
      }
      if (element.components === 'Service') {
        ServicePoint = ServicePoint + element.point
      }
    }
    // console.log("Participation: "+ParticipationPoint,
    //             "Leadershipe: "+LeadershipPoint,
    //             "Achiement: "+AchievementPoint,
    //             "Enrichment: "+EnrichmentPoint,
    //             "CommunityService: "+CommunityServicePoint,
    //             "Service: "+ServicePoint)

    var participation;
    if (ParticipationPoint >= 25) {
      participation = 25;
    } else {
      participation = ParticipationPoint
    }
    var Total = participation + EnrichmentPoint + AchievementPoint + RepresentationPoint + LeadershipPoint + ServicePoint;
    var grade = "No Grade";

    if (Total >= 60 && ServicePoint >= 6 && (LeadershipPoint >= 12 || AchievementPoint >= 12)) {
      grade = "Platinum";
    } else if (Total >= 50 && ServicePoint >= 4 && (LeadershipPoint >= 8 || AchievementPoint >= 8)) {
      grade = "Gold";
    } else if (Total >= 35 && ServicePoint >= 2 && (LeadershipPoint >= 6 || AchievementPoint >= 6)) {
      grade = "Silver";
    } else if (Total >= 20 && ServicePoint >= 2) {
      grade = "Bronze";
    } else {
      grade = "No Grade";
    }

    // console.log(Total)   
    res.render('staff/viewStudentDetails', {
      user:userData,
      student:userStudent,
      cca:ccaRequests,
      participation: participation,
      enrichment: EnrichmentPoint,
      achievement: AchievementPoint,
      representation: RepresentationPoint,
      leadership: LeadershipPoint,
      service: ServicePoint,
      total: Total,
      grade: grade
    });
  } catch (error) {
    console.log(error.message);
  }
};
const loadccaRequest = async (req, res) => {
  try {
    const id = req.session.userid;

    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.find();
    var arr = []
    for (let i = eventData.length - 1; i >= 0; i--) {
      const element = eventData[i]
      // console.log(id,element.userid)
      if (id == element.userid) {
        const userpost = eventData[i];
        arr.push(userpost)

        // console.log(userpost);
      }
    }
    // console.log(arr)
    res.render("staff/ccaRequest", { user: userData, event: arr })

  } catch (error) {
    console.log(error.message)
  }

}
const loadeventRequest = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });

    res.render("staff/eventRequest", { user: userData })

  } catch (error) {
    console.log(error.message)
  }

}
const createEventRequest = async (req, res) => {

  const user = await User.findById(req.session.userid);
  const userData = await User.findById({ _id: req.session.userid });
  // console.log(req.file.filename)

  try {

    const newEventRequest = new Event({
      eventName: req.body.eventName,
      description: req.body.description,
      supportingDocument: req.file.filename,
      datetime: moment.tz(timezone).format('YYYY-MM-DD HH:mm'),
      status: 'Pending',
      userid: user,

    });

    const savedEventRequest = await newEventRequest.save();

    if (savedEventRequest) {
      const datetime = savedEventRequest.datetime.toLocaleString('en-US', { timeZone: 'Asia/Thimphu' });
      res.render('staff/eventRequest', { user: userData, message: "Event Request Successful", datetime });

    }
    else {
      res.render('staff/eventRequest', { user: userData, message: "Event Request failed" });
    }
  } catch (error) {
    console.log(error.message)
  }
};

const uploadCSV = async (req, res, next) => {
  const userData = await User.findById({ _id: req.session.userid });
  const id = req.session.userid;
  const eventData = await Event.find();
  var arr = [];
  for (let i = eventData.length - 1; i >= 0; i--) {
    const element = eventData[i];
    if (id == element.userid) {
      const userpost = eventData[i];
      arr.push(userpost);
    }
  }
  try {
    const eventid = req.body.eventid;
    const ccaRequest = [];
    let errorOccurred = false; // Flag variable to track errors

    csv()
      .fromFile(req.file.path)
      .then(async (response) => {
        for (let i = 0; i < response.length; i++) {
          try {
            const uID = await User.findOne({ userid: response[i].stdid });
            const pointID = await CCARolepoint.findOne({ role: response[i].role });
            const cca1Data = {
              eventid: eventid,
              userid: uID._id,
              ccarolepointid: pointID._id,
            };
            ccaRequest.push(cca1Data);
          } catch (error) {
            console.log('here: ' + error);
            errorOccurred = true; // Set flag to true if error occurs
            res.render('staff/ccaRequest', { user: userData, event: arr, message: 'Student not found' });
            break; // Stop further processing
          }
        }

        if (!errorOccurred) { // If no errors occurred, proceed with insertMany
          CCA.insertMany(ccaRequest)
            .then(async () => {
              const uploadccaCSV = await Event.findByIdAndUpdate(eventid, {
                cca: req.file.filename,
                datetimecca: moment.tz(timezone).format('YYYY-MM-DD HH:mm'),
              });
              const savedCCARequest = uploadccaCSV.save();
              res.render('staff/ccaRequest', { user: userData, event: arr, message: 'CCA successfully submitted' });
            })
            .catch((error) => {
              if (error.code === 11000) {
                res.render('staff/ccaRequest', { user: userData, event: arr, message: 'Role already exists!! Cannot be the same' });
              } else {
                res.render('staff/ccaRequest', { user: userData, event: arr, message: 'Something went wrong!!! Try again' });
              }
            });
        }
      });
  } catch (error) {
    res.render('staff/ccaRequest', { user: userData, event: arr, message: 'Something went wrong' });
  }
};


const loadviewMore = async (req, res) => {
  try {
    const id = req.query.id;

    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.findOne({ _id: id });


    res.render("staff/viewMore", { user: userData, event: eventData })

  } catch (error) {
    console.log(error.message)
  }

}
const loadSetting = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });

    res.render("staff/staffSetting", { staff: userData })

  } catch (error) {
    console.log(error.message)
  }

}

//update user profile
const updateProfile = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const photo = req.file.filename;
    // console.log(photo)


    if (photo) {
      userData.photo = photo;
      await userData.save();

    } const updatedUser = await User.findByIdAndUpdate(userId, {
      new: true,
      runValidators: true,
    });

    res.render('staff/staffSetting', {
      staff: updatedUser,
      message: "Successfully updated"
    });


  } catch (error) {
    return res.render('staff/staffSetting', {
      staff: userData,
      message: "Something went wrong"
    });

  }

}


//update user profile
const updatePassword = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const confirmPassword = req.body.confirmPassword;

    if (oldPassword && newPassword && confirmPassword) {
      const user = await User.findById(userId).select('+password');

      if (await bcrypt.compare(oldPassword, user.password)) {

        if (newPassword === confirmPassword) {

          const hashedPassword = await securePassword(newPassword);
          user.password = hashedPassword;
          await user.save();

          const updatedUser = await User.findByIdAndUpdate(userId, {
            new: true,
            runValidators: true,
          });

          return res.render('staff/staffSetting', {
            staff: updatedUser,
            message: "Password successfully updated",
          });
        } else {
          return res.render('staff/staffSetting', {
            staff: userData,
            message: "New password and confirm password do not match",
          });
        }
      } else {
        return res.render('staff/staffSetting', {
          staff: userData,
          message: "Old password is incorrect",
        });
      }
    } else {
      return res.render('staff/staffSetting', {
        staff: userData,
        message: "Please provide old password, new password, and confirm password",
      });
    }
  } catch (error) {
    return res.render('staff/staffSetting', {
      staff: userData,
      message: "Something went wrong",
    });
  }
};

// const deleteccafile= async(req,res)=>{
//   try {
//     const id = req.query.id;
//     const attributeToDelete = req.query.cca; // The attribute to delete from the User document

//     await Event.updateOne({ _id: id }, { $unset: { [attributeToDelete]: 1 } });

//     const userData = await User.findById({ _id: req.session.userid });
//     const eventData = await Event.find(id);


//     res.render('staff/ccaRequest', {
//       user: userData,
//       event: eventData,
//       message: "Successfully Deleted",
//     });
//   } catch (error) {
//     console.log(error.message);
//   }
// }
module.exports = {
  loadStaffDashboard,
  loadUserDetails,
  loadViewUserDetails,
  loadccaRequest,
  loadeventRequest,
  createEventRequest,
  uploadCSV,
  loadviewMore,
  loadSetting,
  updatePassword,
  updateProfile,
  loadAbout
  // deleteccafile

}