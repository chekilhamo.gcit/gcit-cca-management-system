const User = require('../models/userModel');;
const CCA = require('../models/ccarolepointModel');
const Event = require('../models/eventModel');

const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const csv = require("csvtojson")
var XLSX = require('xlsx');
const randormstring = require('randomstring');
const path = require('path')
const config = require("../config/config");
const { ifError } = require('assert');
const CCARequest = require('../models/ccaRequestModel')
const mongoose = require('mongoose');

const securePassword = async (password) => {
  try {
    const passwordHash = await bcrypt.hash(password, 10);
    return passwordHash;
  } catch (error) {

    console.log(error.message);

  }
}
const loadRegister = async (req, res) => {
  try {

    res.render('admin/register');

  } catch (error) {
    console.log(error.message);
  }
}

const registerAdmin = async (req, res) => {
  try {
    const spassword = await securePassword(req.body.password);
    const admin = new User({
      name: req.body.name,
      userid: req.body.userid,
      email: req.body.email,
      role: req.body.role,
      password: spassword,
      photo: req.file.filename,
    });
    const adminData = await admin.save();

    if (adminData) {
      //verify
      // sendVerifyMail(req.body.name, req.body.email, adminData._id);
      res.render('admin/register', { message: "Your registration has been successful!!" });

    }
    else {
      res.render('admin/register', { message: "Your registration has been failed " });
    }

  } catch (error) {

    console.log(error.message)

  }
}


const loadDashboard = async (req, res) => {
  try {
    const userData0 = await User.findById({ _id: req.session.userid });
    const users = await User.find({ role: "student" });
    const staff = await User.find({ role: "staff" });
    res.render('admin/adminDashboard', {
      admin: userData0,
      users: users,
      staff: staff
    });

  } catch (error) {

    console.log(error.message);

  }
}

const loadstaffManage = async (req, res) => {
  try {
    const userData0 = await User.findById({ _id: req.session.userid });
    const users = await User.find({ role: "student" });
    const staff = await User.find({ role: "staff" });
    res.render('admin/staffManage', {
      admin: userData0,
      users: users,
      staff: staff
    });

  } catch (error) {

    console.log(error.message);

  }
}

const loadIndividualRegisteration = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    res.render('admin/addIndividual', { admin: userData });


  } catch (error) {
    console.log(error.message)
  }
}
const IndividualRegisterationStudent = async (req, res) => {
  try {
    const name = req.body.name;
    const userid = req.body.sid;
    const joinedYear = req.body.joinedYear;
    const email = req.body.email;
    const course = req.body.course;
    const year = req.body.year;
    const password = req.body.sid;

    const finduser = await User.findOne({ userid: userid });
    const findemail = await User.findOne({ email: email });

    const adminData = await User.findById({ _id: req.session.userid });

    if (finduser) {
      res.render('admin/addIndividual', { admin: adminData, message: "Already registered" });
    }
    else if (findemail) {
      res.render('admin/addIndividual', { admin: adminData, message: "Email already used" });

    }
    else {
      const spassword = await securePassword(password);
      const user = new User({
        name: name,
        userid: userid,
        joinedYear: joinedYear,
        email: email,
        course: course,
        year: year,
        password: spassword,
        role: "student",

      });
      const userData = await user.save();
      res.render('admin/addIndividual', { admin: adminData, message: "Registeration Successfull" })
    }


  } catch (error) {

    console.log(error.message);

  }
}
// const loadManageStaff = async(req,res) => {
//     try {
//         const userData = await Staff.find();
//         res.render('ManageStaff',{users:userData});

//     } catch (error) {

//         console.log(error.message);

//     }
// }
const loadIndividualRegisterationStaff = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    res.render('admin/addIndividualStaff', { admin: userData });



  } catch (error) {
    console.log(error.message)
  }
}
const IndividualRegisterationStaff = async (req, res) => {
  try {
    // console.log(req);
    const name = req.body.name;
    const userid = req.body.userid;
    const email = req.body.email;
    const joinedYear = req.body.joinedYear;
    const designation = req.body.designation;
    const password = req.body.userid;

    const finduser = await User.findOne({ userid: userid });
    const findemail = await User.findOne({ email: email });

    const adminData = await User.findById({ _id: req.session.userid });
    if (finduser) {
      res.render('admin/addIndividualStaff', { admin: adminData, message: "Already registered" });
    }
    else if (findemail) {
      res.render('admin/addIndividualStaff', { admin: adminData, message: "Email already used" });

    }

    else {
      const spassword = await securePassword(password);
      const user = new User({
        name: name,
        userid: userid,
        email: email,
        joinedYear: joinedYear,
        designation: designation,
        password: spassword,
        role: "staff"

      });
      const userData = await user.save();
      res.render('admin/addIndividualStaff', { admin: adminData, message: "Registeration successfull" })
    }


  } catch (error) {

    console.log(error.message);

  }
}
const loadStudentBulk = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    res.render('admin/studentBulk', { admin: userData });

  } catch (error) {
    console.log(error.message)
  }


}


const studentBulk = async (req, res) => {
  const adminData = await User.findById({ _id: req.session.userid });

  try {
    const userData = [];
    csv()
      .fromFile(req.file.path)
      .then(async (response) => {
        for (let i = 0; i < response.length; i++) {
          const password = response[i].userid;
          const hashedPassword = await bcrypt.hash(password, 10);
          const studentData = {
            name: response[i].name,
            userid: response[i].userid,
            course: response[i].course,
            email: response[i].email,
            password: hashedPassword, // Replace plain text password with hashed password
            year: response[i].year,
            joinedYear: response[i].joinedYear,
            role: "student"
          };
          userData.push(studentData);
        }

        // Use Mongoose to insert multiple documents
        User.insertMany(userData)
          .then(() => {
            res.render('admin/studentBulk', { admin: adminData, message: 'Registration successful' });
          })
          .catch((error) => {
            // Check for duplicate key error
            if (error.code === 11000) {
              res.render('admin/studentBulk', { admin: adminData, message: 'Student ID already exist!! Cannot be same' });
            } else {
              res.render('admin/studentBulk', { admin: adminData, message: "something went wrong!!! Try again" });
            }
          });
      });
  } catch (error) {
    res.render('admin/studentBulk', { admin: adminData, message: "something went wrong!!! Try again" });

  }
};

const loadStaffBulk = async (req, res) => {
  try {
    const adminData = await User.findById({ _id: req.session.userid });
    res.render("admin/staffBulk", { admin: adminData })

  } catch (error) {
    console.log(error.message)
  }

}
const staffBulk = async (req, res) => {
  const adminData = await User.findById({ _id: req.session.userid });

  try {
    const userData = [];

    csv()
      .fromFile(req.file.path)
      .then(async (response) => {
        for (let i = 0; i < response.length; i++) {
          const password = response[i].userid;
          const hashedPassword = await bcrypt.hash(password, 10);
          const staffData = {
            name: response[i].name,
            userid: response[i].userid,
            email: response[i].email,
            designation: response[i].designation,
            password: hashedPassword,
            joinedYear: response[i].joinedYear,
            role: "staff"

          };

          userData.push(staffData);
        }

        // Use Mongoose to insert multiple documents
        User.insertMany(userData)
          .then(() => {
            res.render('admin/staffBulk', { admin: adminData, message: 'Registeration successful' });

          })
          .catch((error) => {
            // Check for duplicate key error
            if (error.code === 11000) {
              res.render('admin/staffBulk', { admin: adminData, message: 'Staff ID already exist!! Cannot be same' });
            } else {
              res.render('admin/staffBulk', { admin: adminData, message: "something went wrong!!! Try again" });
            }
          });
      });
  } catch (error) {
    res.render('admin/staffBulk', { admin: adminData, message: "Something went wrong!!! Try again" });
  }
};
const loadEditStudent = async (req, res) => {
  try {
    const id = req.query.id;
    const userData1 = await User.findById({ _id: req.session.userid });
    const userData = await User.findById({ _id: id });
    if (userData) {

      res.render('admin/editStudent', { admin: userData1, users: userData });

    }
    else {

      res.redirect('/ManageStudent');

    }
    res.redirect('admin/editStudent');

  } catch (error) {
    console.log(error.message)
  }

}
const updateStudent = async (req, res) => {
  try {
    const id = req.body.id
    const userData1 = await User.findById({ _id: req.session.userid });
    const userData = await User.findByIdAndUpdate({ _id: id }, { $set: { name: req.body.name, userid: req.body.sid, email: req.body.email, joinedYear: req.body.joinedYear, course: req.body.course, year: req.body.year } });
    const userData2 = await User.findById({ _id: id })

    res.render('admin/editStudent', { admin: userData1, users: userData2, message: "Successfully updated" });

  } catch (error) {
    // console.log(error.message);
    const id = req.body.id
    const userData1 = await User.findById({ _id: req.session.userid });
    const userData2 = await User.findById({ _id: id })
    if (error.code === 11000) {
      res.render('admin/editStudent', { admin: userData1, users: userData2, message: 'Email or userid is already registered' });
    } else {
      // Handle other errors
      res.render('admin/editStudent', { admin: userData1, users: userData2, message: 'An error occurred' });
    }

  }
}
const loadccaManage = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    const ccaData = await CCA.find();
    res.render('admin/ccaManage', { admin: userData, cca: ccaData });

  } catch (error) {
    console.log(error.message)
  }

}
const loadstudentprofile = async (req, res) => {
  try {
    const id = req.query.id;
    const userData = await User.findById({ _id: req.session.userid });
    const userData1 = await User.findById({ _id: id });
    const ccaRequests = await CCARequest.aggregate([
      {
        $match: { userid: new mongoose.Types.ObjectId(id) },
      },
      {
        $lookup: {
          from: "events",
          localField: "eventid",
          foreignField: "_id",
          as: "event",
        },
      },
      {
        $unwind: {
          path: "$event",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "userid",
          foreignField: "_id",
          as: "user",
        },
      },
      {
        $unwind: {
          path: "$user",
        },
      },
      {
        $lookup: {
          from: "ccarolepoints",
          localField: "ccarolepointid",
          foreignField: "_id",
          as: "ccarolepoint",
        },
      },
      {
        $unwind: {
          path: "$ccarolepoint",
        },
      },
      {
        $project: {
          eventName: "$event.eventName",
          components: "$ccarolepoint.components",
          roleName: "$ccarolepoint.role",
          point: "$ccarolepoint.points",
          userId: "$user.userid",
          year: "$event.datetimecca"
        },
      }
    ]);
    
    var ParticipationPoint = 0
    var EnrichmentPoint = 0
    var AchievementPoint = 0
    var RepresentationPoint = 0
    var LeadershipPoint = 0
    var ServicePoint = 0

    for (let i = 0; i < ccaRequests.length; i++) {
      const element = ccaRequests[i];
      // console.log(`This is the element array: `+element.components)

      if (element.components === 'Participation') {
        ParticipationPoint = ParticipationPoint + element.point
        // ParticipationPoint = 23;
      }
      if (element.components === 'Enrichment') {
        EnrichmentPoint = EnrichmentPoint + element.point
        // LeadershipPoint = 19;


      }
      if (element.components === 'Achievement') {
        AchievementPoint = AchievementPoint + element.point
        // AchievementPoint = 15;
      }
      if (element.components === 'Representation') {
        RepresentationPoint = RepresentationPoint + element.point
      }
      if (element.components === 'Leadership') {
        LeadershipPoint = LeadershipPoint + element.point
        // CommunityServicePoint = 3;
      }
      if (element.components === 'Service') {
        ServicePoint = ServicePoint + element.point
      }
    }
    // console.log("Participation: "+ParticipationPoint,
    //             "Leadershipe: "+LeadershipPoint,
    //             "Achiement: "+AchievementPoint,
    //             "Enrichment: "+EnrichmentPoint,
    //             "CommunityService: "+CommunityServicePoint,
    //             "Service: "+ServicePoint)

    var participation;
    if (ParticipationPoint >= 25) {
      participation = 25;
    } else {
      participation = ParticipationPoint
    }
    var Total = participation + EnrichmentPoint + AchievementPoint + RepresentationPoint + LeadershipPoint + ServicePoint;
    var grade = "No Grade";

    if (Total >= 60 && ServicePoint >= 6 && (LeadershipPoint >= 12 || AchievementPoint >= 12)) {
      grade = "Platinum";
    } else if (Total >= 50 && ServicePoint >= 4 && (LeadershipPoint >= 8 || AchievementPoint >= 8)) {
      grade = "Gold";
    } else if (Total >= 35 && ServicePoint >= 2 && (LeadershipPoint >= 6 || AchievementPoint >= 6)) {
      grade = "Silver";
    } else if (Total >= 20 && ServicePoint >= 2) {
      grade = "Bronze";
    } else {
      grade = "No Grade";
    }

    // console.log(Total)   
    res.render('admin/studentProfile', {
      admin:userData,
      std:userData1,
      cca:ccaRequests,
      participation: participation,
      enrichment: EnrichmentPoint,
      achievement: AchievementPoint,
      representation: RepresentationPoint,
      leadership: LeadershipPoint,
      service: ServicePoint,
      total: Total,
      grade: grade
    });
  } catch (error) {
    console.log(error.message);
  }
};

const loadstaffprofile = async (req, res) => {
  try {
    const id = req.query.id;
    // console.log(id)
    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.find();
    const userData1 = await User.findById({ _id: id });

    // console.log(eventData)
    console.log()
    var arr = []
    for (let i = eventData.length - 1; i >= 0; i--) {
      const element = eventData[i]
      // console.log(id,element.userid)
      if (id == element.userid) {
        const userpost = eventData[i];
        arr.push(userpost)

        // console.log(userpost);
      }
    }
    // console.log(arr)
    res.render("admin/staffProfile", {
      admin: userData,
      staff: userData1,
      event: arr
    })


  } catch (error) {
    console.log(error.message)
  }

}

const loadsetting = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    res.render('admin/setting', { admin: userData });

  } catch (error) {
    console.log(error.message)
  }

}
const loadccaIndividual = async (req, res) => {
  try {

    const userData = await User.findById({ _id: req.session.userid });
    const userData1 = await CCA.find();
    res.render('admin/ccaIndividual', { admin: userData, cca: userData1 });

  } catch (error) {
    console.log(error.message)
  }

}
const ccaIndividual = async (req, res) => {
  try {
    const role = req.body.role;
    const components = req.body.components;
    const points = req.body.points;

    const adminData = await User.findById({ _id: req.session.userid });
    const findcca = await CCA.findOne({ role: role });
    if (findcca) {
      res.render('admin/ccaIndividual', { admin: adminData, message: "Already registered" });
    }
    else {
      const cca = new CCA({
        role: role,
        components: components,
        points: points,

      });
      const ccaData = await cca.save();

      res.render('admin/ccaIndividual', { admin: adminData, message: "Successfully added " })
    }


  } catch (error) {
    console.log(error.message);
  }
}
const loadccaBulk = async (req, res) => {
  try {

    const userData = await User.findById({ _id: req.session.userid });
    const userData1 = await CCA.find();
    res.render('admin/ccaBulk', { admin: userData, cca: userData1 });

  } catch (error) {
    console.log(error.message)
  }

}
const ccaBulk = async (req, res) => {
  const adminData = await User.findById({ _id: req.session.userid });
  try {
    const ccaData = [];

    csv()
      .fromFile(req.file.path)
      .then((response) => {
        for (let i = 0; i < response.length; i++) {
          const cca1Data = {
            role: response[i].role,
            components: response[i].components,
            points: response[i].points,
          };

          ccaData.push(cca1Data);
        }

        // Use Mongoose to insert multiple documents
        CCA.insertMany(ccaData)
          .then(() => {
            res.render('admin/ccaBulk', { admin: adminData, message: 'Registration successful' });
          })
          .catch((error) => {
            if (error.message.includes('role')) {
              res.render('admin/ccaBulk', { admin: adminData, message: 'Role already exists! Cannot be the same.' });
            } else {
              res.render('admin/ccaBulk', { admin: adminData, message: "Something went wrong!!! Try again" });
            }
          });
      });
  } catch (error) {
    res.render('admin/ccaBulk', { admin: adminData, message: "Something went wrong!!! Try again" });
  }
};

//delete user
const deleteStudentLoad = async (req, res) => {
  try {

    const id = req.query.id;

    await User.deleteOne({ _id: id });
    await CCA.deleteOne({ _id: id });

    const adminData = await User.findById({ _id: req.session.userid });
    const users = await User.find({ role: "student" });
    // const ccaData = await CCA.find();

    res.render('admin/adminDashboard', {
      admin: adminData,
      users: users,
      message: "Successfully deleted"
    });
  } catch (error) {
    console.log(error.message);
  }
}

const deleteStaffLoad = async (req, res) => {
  try {

    const id = req.query.id;

    await User.deleteOne({ _id: id });

    const adminData = await User.findById({ _id: req.session.userid });
    const staff = await User.find({ role: "staff" });
    // const ccaData = await CCA.find();

    res.render('admin/staffManage', {
      admin: adminData,
      staff: staff,
      message: "Successfully Deleted"
    });
  } catch (error) {
    console.log(error.message);
  }
}
const loadEditStaff = async (req, res) => {
  try {
    const id = req.query.id;
    const userData1 = await User.findById({ _id: req.session.userid });
    const userData = await User.findById({ _id: id });
    if (userData) {
      res.render('admin/editStaff', { admin: userData1, staff: userData });
    }
    else {
      res.redirect('/adminDashboard');
    }
    // res.render('admin/editStaff',{admin:userData1,staff:userData});
  } catch (error) {
    console.log(error.message)
  }

}
const updateStaff = async (req, res) => {
  try {
    const id = req.body.id
    // console.log(req.query.id)

    const userData1 = await User.findById({ _id: req.session.userid });
    const userData = await User.findByIdAndUpdate({ _id: id }, { $set: { name: req.body.name, userid: req.body.sid, email: req.body.email, designation: req.body.designation, joinedYear: req.body.joinedYear } });
    // console.log(_id)
    const userData2 = await User.findById({ _id: id })
    res.render('admin/editStaff', { admin: userData1, staff: userData2, message: "Successfully updated" });

  } catch (error) {
    // console.log(error.message);
    const id = req.body.id
    const userData1 = await User.findById({ _id: req.session.userid });
    const userData2 = await User.findById({ _id: id })
    if (error.code === 11000) {
      res.render('admin/editStaff', { admin: userData1, staff: userData2, message: 'Email or userid is already registered' });
    } else {
      // Handle other errors
      res.render('admin/editStaff', { admin: userData1, staff: userData2, message: 'An error occurred' });
    }

  }
}
const loadEditCca = async (req, res) => {
  try {
    const id = req.query.id;
    const userData1 = await User.findById({ _id: req.session.userid });
    const ccaData = await CCA.findById({ _id: id });
    if (ccaData) {

      res.render('admin/editCca', { admin: userData1, cca: ccaData });

    }
    else {

      res.redirect('/ccaManage');

    }

  } catch (error) {
    console.log(error.message)
  }

}
const updateCca = async (req, res) => {
  try {
    const id = req.body.id
    const userData1 = await User.findById({ _id: req.session.userid });
    const ccaData = await CCA.findByIdAndUpdate({ _id: id }, { $set: { role: req.body.role, points: req.body.points, components: req.body.components } });
    const updatedcca = await CCA.findById({ _id: id });

    res.render('admin/editCca', { admin: userData1, cca: updatedcca, message: "Successfully updated" });

  } catch (error) {

    // console.log(error.message)
    const id = req.body.id
    const userData1 = await User.findById({ _id: req.session.userid });
    const updatedcca = await CCA.findById({ _id: id });

    if (error.code === 11000) {
      res.render('admin/editCca', { admin: userData1, cca: updatedcca, message: "Role already exist" });

    }
    else {
      res.render('admin/editCca', { admin: userData1, cca: updatedcca, message: "Something went wrong" });

    }

  }
}

//delete user
const deleteCca = async (req, res) => {
  try {

    const id = req.query.id;
    await CCA.deleteOne({ _id: id });
    const adminData = await User.findById({ _id: req.session.userid });
    const ccaData = await CCA.find();



    res.render('admin/ccaManage', {
      admin: adminData,
      cca: ccaData,
      message: "Successfully Deleted"
    });
  } catch (error) {

    console.log(error.message);

  }
}


//update user profile
const updateProfile = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const photo = req.file.filename;

    if (photo) {
      userData.photo = photo;
      await userData.save();

    } const updatedUser = await User.findByIdAndUpdate(userId, {
      new: true,
      runValidators: true,
    });

    res.render('admin/setting', {
      admin: updatedUser,
      message: "Successfully updated"
    });


  } catch (error) {
    return res.render('admin/setting', {
      admin: userData,
      message: "Something went wrong"
    });

  }

}


//update user profile
const updatePassword = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const confirmPassword = req.body.confirmPassword;

    if (oldPassword && newPassword && confirmPassword) {
      const user = await User.findById(userId).select('+password');

      if (await bcrypt.compare(oldPassword, user.password)) {

        if (newPassword === confirmPassword) {
          const hashedPassword = await securePassword(newPassword);
          user.password = hashedPassword;
          await user.save();

          const updatedUser = await User.findByIdAndUpdate(userId, {
            new: true,
            runValidators: true,
          });

          return res.render('admin/setting', {
            admin: updatedUser,
            message: "Password successfully updated",
          });
        } else {
          return res.render('admin/setting', {
            admin: userData,
            message: "New password and confirm password do not match",
          });
        }
      } else {
        return res.render('admin/setting', {
          admin: userData,
          message: "Old password is incorrect",
        });
      }
    } else {
      return res.render('admin/setting', {
        admin: userData,
        message: "Please provide old password, new password, and confirm password",
      });
    }
  } catch (error) {
    return res.render('admin/setting', {
      admin: userData,
      message: "Something went wrong",
    });
  }
};
const loadViewMoreEvent = async (req, res) => {
  try {
    const id = req.query.id;
    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.findById({ _id: id });
    // res.local.data = { id: req.query.id}

    res.render("admin/eventDetails", { admin: userData, event: eventData })

  } catch (error) {
    console.log(error.message)
  }

}
const loadAbout = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    console.log(userData)

    res.render("admin/about", { user: userData })

  } catch (error) {
    console.log(error.message)
  }
}
module.exports = {
  loadRegister,
  registerAdmin,
  loadDashboard,
  loadstaffManage,
  // loadManageStudent,
  loadIndividualRegisteration,
  IndividualRegisterationStudent,
  // loadManageStaff,
  loadIndividualRegisterationStaff,
  IndividualRegisterationStaff,
  loadStudentBulk,
  studentBulk,
  loadStaffBulk,
  staffBulk,
  loadEditStudent,
  updateStudent,
  loadccaManage,
  loadsetting,
  loadstudentprofile,
  loadstaffprofile,
  loadccaIndividual,
  ccaIndividual,
  loadccaBulk,
  ccaBulk,
  deleteStudentLoad,
  deleteStaffLoad,
  loadEditStaff,
  updateStaff,
  loadEditCca,
  updateCca,
  deleteCca,
  // loadStudentDashboard,
  // // verificationLoad,
  // // sentVerificationLi,nk
  // userProfileLoad,
  // editUserProfileLoad,
  updateProfile,
  updatePassword,
  loadViewMoreEvent,
  loadAbout
}
