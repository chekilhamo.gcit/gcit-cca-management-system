const User = require('../models/userModel');
const bcrypt= require('bcrypt');
const nodemailer = require('nodemailer');
const randormstring = require('randomstring');
const AppError = require('./../utils/appError')
 
const config = require ("../config/config");

var OTP = ''

const securePassword = async(password)=>{
    try {
        const passwordHash = await bcrypt.hash(password, 10);
        return passwordHash;
    } catch (error) {

        console.log(error.message);
        
    }

}

//for reset password send mail
const sendResetPasswordMail = async(name, email, token)=>{
    try {
        console.log("This is inside SendResetpassword fucntion"+ name,email,token)
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailPassword
            }

        });
        const mailOptions = {
            from:config.emailUser,
            to:email,
            subject:'Reset Password',
            html:`<p>Hii `+name+`, 
                 Copy the OTP and paste it in OTP Field </p><br>
                 <h1>`+token+`<h1>`
        }
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:- ",info.response);
            }
        });

    } catch (error) {

        console.log(error.message);
        
    }
}

const loadRegister = async(req,res)=>{
    try{

        res.render('admin/register');

    }catch(error){
        console.log(error.message);
    }
}

//login user method
const loginLoad = async(req, res )=>{
    try {
        
        res.render('admin/login');

    } catch (error) {

        console.log(error.message)
        
    }
}
// user logout
const logout = async(req, res)=>{
    try {

        req.session.destroy();
        res.redirect('/');

        
    } catch (error) {

        console.log(error.message);
        
    }
}

//forget password 
const forgetLoad = async(req, res)=>{
    try {

        res.render('admin/forget');
        
    } catch (error) {

        console.log(error.message);
        
    }
}
//forget password 
const loadOTP = async(req, res)=>{
    try {

        res.render('admin/otp');
        
    } catch (error) {

        console.log(error.message);
        
    }
}


const forgetVerify = async(req, res)=>{
    try {
        const email = req.body.email;
        console.log(email)
        const userData = await User.findOne({email:email});
        if(userData){
            const randomString = randormstring.generate(6);
            OTP = randomString;
            const updatedData = await User.updateOne({email:email},{$set:{token:randomString}});
            sendResetPasswordMail(userData.name, userData.email, randomString);
            res.redirect('/otp');
        }
        else{
            res.render('admin/forget', {message:"User email is incorrect"});
        }
    } catch (error) {

        console.log(error.message);
        
    }
}

const enterOTP = async(req, res)=>{
    try {
        const user_recieved_OTP = req.body.OTP;
        // console.log("THis is inside ENter opt: "+user_recieved_OTP)
        if(user_recieved_OTP === OTP){
            res.redirect('/reset-password')
        }
        else{
            res.render('admin/otp',{message:'Please check the OTP'})
        }
    } catch (error) {
        
    }
}

//load forget passward page
const resetPasswordLoad = async(req,res)=>{
    try {
        res.render('admin/reset-password')
        
    } catch (error) {
        
    }
}

// const restrictTo = (...roles) => {
//     return async(req, res, next) => {
//         // console.log(req.session.userid)
//         const user = await User.findOne({userid:req.session.userid});
//         console.log(user)
//       if (!roles.includes(user.role)) {
//         return next(
//           new AppError('You do not have permission to perform this action', 403)
//         );
//       }
//       next();
//     };
//   };

  
//reset password post 
const resetPassword = async(req, res)=>{
    // console.log("THis is me from reset passwrod")
    try {
        const tokenData  = await User.findOne({token:OTP});
        // console.log(tokenData)
        if(tokenData){
            const password = req.body.password;
            const cpassword = req.body.cpassword;
            if(password===cpassword){
                 // console.log("This is the new Password "+password);
                const secure_password = await securePassword(cpassword);

                const updatedData = await User.findByIdAndUpdate({_id:tokenData._id},{$set:{password:secure_password, token:''}},{new:true});

                res.redirect('/');

            }
            else{
                res.redirect('/reset-Password')
            }
           
        }        
    } catch (error) {

        console.log(error.message);
        
    }
}

//verify logi
const verifyLogin = async (req, res) => {
    try {
      const userid = req.body.userid;
      const password = req.body.password;
  
      const userData = await User.findOne({ userid: userid });
  
      if (userData) {
        const passwordMatch = await bcrypt.compare(password, userData.password);
  
        if (passwordMatch) {
          if (userData.role === "student") {
            req.session.userid = userData._id;
            req.session.role = userData.role;
            return res.redirect("/studentDashboard");
          } 
          else if (userData.role === "admin") {
            req.session.userid = userData._id;
            req.session.role = userData.role;
            return res.redirect("/adminDashboard");
          } 
          else if (userData.role === "staff")
           {
            req.session.userid = userData._id;
            req.session.role = userData.role;
            return res.redirect("/staffDashboard");
          } 
          else if (userData.role === "hos") {

            req.session.userid = userData._id;
            req.session.role = userData.role;
            return res.redirect("/hosDashboard");

          } 
          else {

            res.render("admin/login", { message: "User_id or Password is incorrect!" });
          }
        } else {
          res.render("admin/login", { message: "User_id or Password is incorrect!" });
        }
      } else {
        res.render("admin/login", { message: "User_id or Password is incorrect!" });
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  const restrictTo = (...roles) => {
    return (req, res, next) => {
      if (!req.session.userid || !roles.includes(req.session.role)) {
        return next(
          new AppError('You do not have permission to perform this action', 403)
        )
      }
      next()
    }
  }
  

  
module.exports={
    loadRegister,
    loginLoad,
    verifyLogin,
    forgetLoad,
    forgetVerify,
    resetPasswordLoad,
    resetPassword,
    loadOTP,
    enterOTP,
    logout,
    restrictTo
}