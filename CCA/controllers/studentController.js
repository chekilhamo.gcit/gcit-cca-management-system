const User = require('../models/userModel');
const CCARequest = require('../models/ccaRequestModel')
const mongoose = require('mongoose');
const bcrypt = require('bcrypt')

const securePassword = async (password) => {
  try {
    const passwordHash = await bcrypt.hash(password, 10);
    return passwordHash;
  } catch (error) {

    console.log(error.message);
  }
}

const loadStudentDashboard = async (req, res) => {
  try {
    const userId = req.session.userid;
    const userData = await User.findById({ _id: userId });
    const ccaRequests = await CCARequest.aggregate([
      {
        $match: { userid: new mongoose.Types.ObjectId(userId) },
      },
      {
        $lookup: {
          from: "events",
          localField: "eventid",
          foreignField: "_id",
          as: "event",
        },
      },
      {
        $unwind: {
          path: "$event",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "userid",
          foreignField: "_id",
          as: "user",
        },
      },
      {
        $unwind: {
          path: "$user",
        },
      },
      {
        $lookup: {
          from: "ccarolepoints",
          localField: "ccarolepointid",
          foreignField: "_id",
          as: "ccarolepoint",
        },
      },
      {
        $unwind: {
          path: "$ccarolepoint",
        },
      },
      {
        $project: {
          eventName: "$event.eventName",
          components: "$ccarolepoint.components",
          status: "$event.ccaStatus",
          roleName: "$ccarolepoint.role",
          point: "$ccarolepoint.points",
          userId: "$user.userid",
          year: "$event.datetimecca"
        },
      }
    ]);
    // console.log(ccaRequests)
    var ParticipationPoint = 0
    var EnrichmentPoint = 0
    var AchievementPoint = 0
    var RepresentationPoint = 0
    var LeadershipPoint = 0
    var ServicePoint = 0

    for (let i = 0; i < ccaRequests.length; i++) {
      const element = ccaRequests[i];
      // console.log(`This is the element array: `+element.components)

      if (element.components === 'Participation') {
        ParticipationPoint = ParticipationPoint + element.point
        // ParticipationPoint = 23;
      }
      if (element.components === 'Enrichment') {
        EnrichmentPoint = EnrichmentPoint + element.point
        // LeadershipPoint = 19;


      }
      if (element.components === 'Achievement') {
        AchievementPoint = AchievementPoint + element.point
        // AchievementPoint = 15;
      }
      if (element.components === 'Representation') {
        RepresentationPoint = RepresentationPoint + element.point
      }
      if (element.components === 'Leadership') {
        LeadershipPoint = LeadershipPoint + element.point
        // CommunityServicePoint = 3;
      }
      if (element.components === 'Service') {
        ServicePoint = ServicePoint + element.point
      }
    }

     //Pints needed to achieve Platinum
     var par = "-";
     var enrich = "-";
     var repre = "-";
     var achiev = 0;
     if(AchievementPoint >= 12 || LeadershipPoint >= 12){
      achiev = "* Points Achieved";
     } else
     achiev = 12 - AchievementPoint;
     
     var leader = 0;
     if (AchievementPoint >= 12 || LeadershipPoint >= 12){
      leader = "* Points Achieved"
     } else {
      leader = 12 - LeadershipPoint
     }

     var ser = 0;
     if (ServicePoint >=6){
      ser = "Points Achieved"
     } else {
      var ser = 6 - ServicePoint;
     }
    


    var participation;
    if (ParticipationPoint >= 25) {
      participation = 25;
    } else {
      participation = ParticipationPoint
    }
    var Total = participation + EnrichmentPoint + AchievementPoint + RepresentationPoint + LeadershipPoint + ServicePoint;
    var grade = "No Grade";

    if (Total >= 60 && (achievement >= 12 || LeadershipPoint >= 12) && ServicePoint >= 6) {
      grade = "Platinum";
    } else if (Total >= 50 && ServicePoint >= 4 && (LeadershipPoint >= 8 || AchievementPoint >= 8)) {
      grade = "Gold";
    } else if (Total >= 35 && ServicePoint >= 2 && (LeadershipPoint >= 6 || AchievementPoint >= 6)) {
      grade = "Silver";
    } else if (Total >= 20 && ServicePoint >= 2) {
      grade = "Bronze";
    } else {
      grade = "No Grade";
    }
    tot = 60 - Total;

    // console.log(Total)   
    res.render('student/studentDashboard', {
      std: userData,
      participation: participation,
      enrichment: EnrichmentPoint,
      achievement: AchievementPoint,
      representation: RepresentationPoint,
      leadership: LeadershipPoint,
      service: ServicePoint,
      total: Total,
      grade: grade,
      par,
      enrich,
      achiev,
      repre,
      leader,
      ser
    });
  } catch (error) {
    console.log(error.message);
  }
};

const loadStudentSetting = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });

    res.render("student/studentSetting", { std: userData })

  } catch (error) {
    console.log(error.message)
  }

}

const loadAbout = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    console.log(userData)

    res.render("student/about", { std: userData })

  } catch (error) {
    console.log(error.message)
  }

}
const loadcca = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    const ccaRequests = await CCARequest.aggregate([
      {
        $match: { userid: new mongoose.Types.ObjectId(req.session.userid) },
      },
      {
        $lookup: {
          from: "events",
          localField: "eventid",
          foreignField: "_id",
          as: "event",
        },
      },
      {
        $unwind: {
          path: "$event",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "userid",
          foreignField: "_id",
          as: "user",
        },
      },
      {
        $unwind: {
          path: "$user",
        },
      },
      {
        $lookup: {
          from: "ccarolepoints",
          localField: "ccarolepointid",
          foreignField: "_id",
          as: "ccarolepoint",
        },
      },
      {
        $unwind: {
          path: "$ccarolepoint",
        },
      },
      {
        $project: {
          eventName: "$event.eventName",
          ccaStatus: "$event.ccaStatus",
          components: "$ccarolepoint.components",
          roleName: "$ccarolepoint.role",
          point: "$ccarolepoint.points",
          userId: "$user.userid",
          year: "$event.datetimecca"
        },
      }
    ]);


    if (ccaRequests.length === 0) {
      res.render("student/cca", {
        std: userData,
        cca: ccaRequests,
        year: 0,
        percentage: 0
      })
    } else {
      const date = ccaRequests[0].year;

      // console.log(ccaRequests)
      const year = new Date(date).getFullYear()
      console.log(year)

      // console.log(year)
      // calculate total points and maximum points
      let totalPoints = 0;
      let maxPoints = 0;
      ccaRequests.forEach(request => {
        totalPoints += request.point;
        maxPoints += request.ccaStatus.maxPoints;
      });

      // calculate percentage
      const percentage = Math.round((totalPoints / maxPoints) * 100);

      // console.log(ccaRequests)
      res.render("student/cca", {
        std: userData,
        cca: ccaRequests,
        year: year,
        percentage: percentage
      })
    }
  } catch (error) {
    console.log(error.message)
  }
}


//update user profile
const updateProfile = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const photo = req.file.filename;
    if (photo) {
      userData.photo = photo;
      await userData.save();

    } const updatedUser = await User.findByIdAndUpdate(userId, {
      new: true,
      runValidators: true,
    });

    res.render('student/studentSetting', {
      std: updatedUser,
      message: "Successfully updated"
    });

  } catch (error) {
    return res.render('student/studentSetting', {
      std: userData,
      message: "Something went wrong"
    });

  }

}


//update user profile
const updatePassword = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const confirmPassword = req.body.confirmPassword;

    if (oldPassword && newPassword && confirmPassword) {
      const user = await User.findById(userId).select('+password');

      if (await bcrypt.compare(oldPassword, user.password)) {

        if (newPassword === confirmPassword) {
          const hashedPassword = await securePassword(newPassword);
          user.password = hashedPassword;
          await user.save();

          const updatedUser = await User.findByIdAndUpdate(userId, {
            new: true,
            runValidators: true,
          });

          return res.render('student/studentSetting', {
            std: updatedUser,
            message: "Password successfully updated",
          });
        } else {
          return res.render('student/studentSetting', {
            std: userData,
            message: "New password and confirm password do not match",
          });
        }
      } else {
        return res.render('student/studentSetting', {
          std: userData,
          message: "Old password is incorrect",
        });
      }
    } else {
      return res.render('student/studentSetting', {
        std: userData,
        message: "Please provide old password, new password, and confirm password",
      });
    }
  } catch (error) {
    return res.render('student/studentSetting', {
      std: userData,
      message: "Something went wrong",
    });
  }
};
module.exports = {
  loadStudentDashboard,
  loadStudentSetting,
  loadcca,
  updateProfile,
  updatePassword,
  loadAbout
}