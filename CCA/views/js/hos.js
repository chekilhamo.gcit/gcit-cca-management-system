// SIDEBAR DROPDOWN
const allDropdown = document.querySelectorAll('#sidebar .side-dropdown');
const sidebar = document.getElementById('sidebar');

allDropdown.forEach(item=> {
	const a = item.parentElement.querySelector('a:first-child');
	a.addEventListener('click', function (e) {
		e.preventDefault();

		if(!this.classList.contains('active')) {
			allDropdown.forEach(i=> {
				const aLink = i.parentElement.querySelector('a:first-child');

				aLink.classList.remove('active');
				i.classList.remove('show');
			})
		}

		this.classList.toggle('active');
		item.classList.toggle('show');
	})
})





// SIDEBAR COLLAPSE
const toggleSidebar = document.querySelector('nav .toggle-sidebar');
const allSideDivider = document.querySelectorAll('#sidebar .divider');

if(sidebar.classList.contains('hide')) {
	allSideDivider.forEach(item=> {
		item.textContent = '-'
	})
	allDropdown.forEach(item=> {
		const a = item.parentElement.querySelector('a:first-child');
		a.classList.remove('active');
		item.classList.remove('show');
	})
} else {
	allSideDivider.forEach(item=> {
		item.textContent = item.dataset.text;
	})
}

toggleSidebar.addEventListener('click', function () {
	sidebar.classList.toggle('hide');

	if(sidebar.classList.contains('hide')) {
		allSideDivider.forEach(item=> {
			item.textContent = '-'
		})

		allDropdown.forEach(item=> {
			const a = item.parentElement.querySelector('a:first-child');
			a.classList.remove('active');
			item.classList.remove('show');
		})
	} else {
		allSideDivider.forEach(item=> {
			item.textContent = item.dataset.text;
		})
	}
})




sidebar.addEventListener('mouseleave', function () {
	if(this.classList.contains('hide')) {
		allDropdown.forEach(item=> {
			const a = item.parentElement.querySelector('a:first-child');
			a.classList.remove('active');
			item.classList.remove('show');
		})
		allSideDivider.forEach(item=> {
			item.textContent = '-'
		})
	}
})



sidebar.addEventListener('mouseenter', function () {
	if(this.classList.contains('hide')) {
		allDropdown.forEach(item=> {
			const a = item.parentElement.querySelector('a:first-child');
			a.classList.remove('active');
			item.classList.remove('show');
		})
		allSideDivider.forEach(item=> {
			item.textContent = item.dataset.text;
		})
	}
})




// PROFILE DROPDOWN
const profile = document.querySelector('nav .profile');
const imgProfile = profile.querySelector('img');
const dropdownProfile = profile.querySelector('.profile-link');

imgProfile.addEventListener('click', function () {
	dropdownProfile.classList.toggle('show');
})




// MENU
const allMenu = document.querySelectorAll('main .content-data .head .menu');

allMenu.forEach(item=> {
	const icon = item.querySelector('.icon');
	const menuLink = item.querySelector('.menu-link');

	icon.addEventListener('click', function () {
		menuLink.classList.toggle('show');
	})
})



window.addEventListener('click', function (e) {
	if(e.target !== imgProfile) {
		if(e.target !== dropdownProfile) {
			if(dropdownProfile.classList.contains('show')) {
				dropdownProfile.classList.remove('show');
			}
		}
	}

	allMenu.forEach(item=> {
		const icon = item.querySelector('.icon');
		const menuLink = item.querySelector('.menu-link');

		if(e.target !== icon) {
			if(e.target !== menuLink) {
				if (menuLink.classList.contains('show')) {
					menuLink.classList.remove('show')
				}
			}
		}
	})
})

//pagination
var tbody = document.querySelector("tbody");
	var pageUl = document.querySelector(".pagination");
	var itemShow = document.querySelector("#itemperpage");
	var tr = tbody.querySelectorAll("tr");
	var emptyBox = [];
	var index = 1;
	var itemPerPage = 8;

	for(let i=0; i<tr.length; i++){ emptyBox.push(tr[i]);}

	itemShow.onchange = giveTrPerPage;
	function giveTrPerPage(){
		itemPerPage = Number(this.value);
		// console.log(itemPerPage);
		displayPage(itemPerPage);
		pageGenerator(itemPerPage);
		getpagElement(itemPerPage);
	}

	function displayPage(limit){
		tbody.innerHTML = '';
		for(let i=0; i<limit; i++){
			tbody.appendChild(emptyBox[i]);
		}
		const  pageNum = pageUl.querySelectorAll('.list');
		pageNum.forEach(n => n.remove());
	}
	displayPage(itemPerPage);

	function pageGenerator(getem){
		const num_of_tr = emptyBox.length;
		if(num_of_tr <= getem){
			pageUl.style.display = 'none';
		}else{
			pageUl.style.display = 'flex';
			const num_Of_Page = Math.ceil(num_of_tr/getem);
			for(i=1; i<=num_Of_Page; i++){
				const li = document.createElement('li'); li.className = 'list';
				const a =document.createElement('a'); a.href = '#'; a.innerText = i;
				a.setAttribute('data-page', i);
				li.appendChild(a);
				pageUl.insertBefore(li,pageUl.querySelector('.next'));
			}
		}
	}
	pageGenerator(itemPerPage);
	let pageLink = pageUl.querySelectorAll("a");
	let lastPage =  pageLink.length - 2;

	function pageRunner(page, items, lastPage, active){
		for(button of page){
			button.onclick = e=>{
				const page_num = e.target.getAttribute('data-page');
				const page_mover = e.target.getAttribute('id');
				if(page_num != null){
					index = page_num;

				}else{
					if(page_mover === "next"){
						index++;
						if(index >= lastPage){
							index = lastPage;
						}
					}else{
						index--;
						if(index <= 1){
							index = 1;
						}
					}
				}
				pageMaker(index, items, active);
			}
		}

	}
	var pageLi = pageUl.querySelectorAll('.list'); pageLi[0].classList.add("active");
	pageRunner(pageLink, itemPerPage, lastPage, pageLi);

	function getpagElement(val){
		let pagelink = pageUl.querySelectorAll("a");
		let lastpage =  pagelink.length - 2;
		let pageli = pageUl.querySelectorAll('.list');
		pageli[0].classList.add("active");
		pageRunner(pagelink, val, lastpage, pageli);

	}



	function pageMaker(index, item_per_page, activePage){
		const start = item_per_page * index;
		const end  = start + item_per_page;
		const current_page =  emptyBox.slice((start - item_per_page), (end-item_per_page));
		tbody.innerHTML = "";
		for(let j=0; j<current_page.length; j++){
			let item = current_page[j];
			tbody.appendChild(item);
		}
		Array.from(activePage).forEach((e)=>{e.classList.remove("active");});
			activePage[index-1].classList.add("active");
	}


// PROGRESSBAR
const allProgress = document.querySelectorAll('main .card .progress');

allProgress.forEach(item=> {
	item.style.setProperty('--value', item.dataset.value)
})

//Reject Remarks
function rejectRemarks() {
	var blur=document.getElementById('blur');
	blur.classList.toggle('active');
	var popup = document.getElementById('popup');
	popup.classList.toggle('active');
	var a = document.getElementById('popupLink')
	a.setAttribute('href', '/reject-event?id='+id)
	console.log(id)
	return false
	
}
function hidePopup() {
	var blur = document.getElementById('blur');
	blur.classList.toggle('active');
	var popup = document.getElementById('popup');
	popup.classList.toggle('active');
	return false; // Prevent form submission
}

// USER DETAILS.HTML
// for the students management tab(dropdowns)
const dropdowns = document.querySelectorAll('.dropdown');

dropdowns.forEach(dropdown => {
  const select = dropdown.querySelector('.select');
  const caret = dropdown.querySelector('.caret');
  const menu = dropdown.querySelector('.menu');
  const options = dropdown.querySelectorAll('.menu li');
  const selected = dropdown.querySelector('.selected');

  select.addEventListener('click', () => {
    select.classList.toggle('select-clicked');
    caret.classList.toggle('caret-rotate');
    menu.classList.toggle('menu-open');
  });

  options.forEach(option => {
    option.addEventListener('click', () =>{
      selected.innerText = option.innerText;
      select.classList.remove('select-clicked');
      caret.classList.remove('caret-rotate');
      menu.classList.remove('menu-open');
      options.forEach(option => {
        option.classList.remove('active');
      });

      option.classList.add('active');
    });
  });
});
// For Table Search
const search = document.querySelector('.input-group input'),
    table_rows = document.querySelectorAll('tbody tr'),
    table_headings = document.querySelectorAll('thead th');

// 1. Searching for specific data of HTML table
search.addEventListener('input', searchTable);

function searchTable() {
    table_rows.forEach((row, i) => {
        let table_data = row.textContent.toLowerCase(),
            search_data = search.value.toLowerCase();

        row.classList.toggle('hide', table_data.indexOf(search_data) < 0);
        row.style.setProperty('--delay', i / 25 + 's');
    })

    document.querySelectorAll('tbody tr:not(.hide)').forEach((visible_row, i) => {
        visible_row.style.backgroundColor = (i % 2 == 0) ? 'transparent' : '#0000000b';
    });
}

// 2. Sorting | Ordering data of HTML table

// table_headings.forEach((head, i) => {
//     let sort_asc = true;
//     head.onclick = () => {
//         table_headings.forEach(head => head.classList.remove('active'));
//         head.classList.add('active');

//         document.querySelectorAll('td').forEach(td => td.classList.remove('active'));
//         table_rows.forEach(row => {
//             row.querySelectorAll('td')[i].classList.add('active');
//         })

//         head.classList.toggle('asc', sort_asc);
//         sort_asc = head.classList.contains('asc') ? false : true;

//         sortTable(i, sort_asc);
//     }
// })


// function sortTable(column, sort_asc) {
//     [...table_rows].sort((a, b) => {
//         let first_row = a.querySelectorAll('td')[column].textContent.toLowerCase(),
//             second_row = b.querySelectorAll('td')[column].textContent.toLowerCase();

//         return sort_asc ? (first_row < second_row ? 1 : -1) : (first_row < second_row ? -1 : 1);
//     })
//         .map(sorted_row => document.querySelector('tbody').appendChild(sorted_row));
// }




// Request List