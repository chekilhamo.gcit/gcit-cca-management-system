var grade = document.getElementById("grade")
// grade.innerHTML = "Your Grade : Silver";

var participation = document.getElementById('participation').innerHTML
var leadership = document.getElementById('enrichment').innerHTML
var achievement = document.getElementById('achievement').innerHTML
var enrichment = document.getElementById('representation').innerHTML
var community = document.getElementById('leadership').innerHTML
var service = document.getElementById('service').innerHTML
var total = document.getElementById('total').innerHTML

var award = document.getElementById('award')

var percentage;


if(total>=60 && (achievement >= 12 || leadership >= 12 ) && community >= 6){
  console.log("this is in platinium ")
  grade.innerHTML = "Grade : Platinum";
  award.innerHTML =`<img src="../img/icon.png" alt="" id="award"class="avatar-title  ">`;
  percentage = total

  var colors = ["#CC7000"],
    dataColors = $("#basic-radialbar").data("colors");
    dataColors && (colors = dataColors.split(","));
    var options = {
            chart: {
                height: 320,
                type: "radialBar"
            },
            plotOptions: {
                radialBar: {
                    hollow: {
                        size: "70%"
                    }
                }
            },
            colors: colors,
            series: [percentage],
            labels: ["Platinum"]
        },
        chart = new ApexCharts(document.querySelector("#basic-radialbar"), options);
        chart.render();

}
else if(total>=50 && (achievement >= 8 || leadership >= 8) && service >= 2){
    if(total>59){
        total=59
        grade.innerHTML = "Grade : Gold";
      award.innerHTML =`<img src="../img/gold.png" alt="" id="award"class="avatar-title ">`;
      percentage1 = (total)/60*100
      percentage = Math.round(percentage1);

      var colors = ["#CC7000"],
        dataColors = $("#basic-radialbar").data("colors");
        dataColors && (colors = dataColors.split(","));
        var options = {
                chart: {
                    height: 320,
                    type: "radialBar"
                },
                plotOptions: {
                    radialBar: {
                        hollow: {
                            size: "70%"
                        }
                    }
                },
                colors: colors,
                series: [percentage],
                labels: ["Gold"]
            },
            chart = new ApexCharts(document.querySelector("#basic-radialbar"), options);
        chart.render();

        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Achievement","Leadership", "Community service"],
            datasets: [{
            label: 'pooints achieved',
            data: [achievement,leadership ,community],
            backgroundColor: "rgba(153,255,51,1)"
            }, {
            label: 'Points to be achieved',
            data: [12,12,6],
            backgroundColor: "#CC7000"
            }]
        }
        });


    }
    else{
        grade.innerHTML = "Grade : Gold";
      award.innerHTML =`<img src="../img/gold.png" alt="" id="award"class="avatar-title ">`;
      percentage1 = (total)/60*100
      percentage = Math.round(percentage1);

      var colors = ["#CC7000"],
        dataColors = $("#basic-radialbar").data("colors");
        dataColors && (colors = dataColors.split(","));
        var options = {
                chart: {
                    height: 320,
                    type: "radialBar"
                },
                plotOptions: {
                    radialBar: {
                        hollow: {
                            size: "70%"
                        }
                    }
                },
                colors: colors,
                series: [percentage],
                labels: ["Gold"]
            },
            chart = new ApexCharts(document.querySelector("#basic-radialbar"), options);
        chart.render();



    }
      

}
else if(total>=35){
   
    if(total>49){
  
        total=49
        grade.innerHTML = "Grade : Silver";
        award.innerHTML =`<img src="../img/silver.png" alt="" id="award"class="avatar-title">`;
        percentage1 = (total/50)*100
        percentage = Math.round(percentage1);
        console.log(percentage)
        var colors = ["#CC7000"],
            dataColors = $("#basic-radialbar").data("colors");
            dataColors && (colors = dataColors.split(","));
            var options = {
                    chart: {
                        height: 320,
                        type: "radialBar"
                    },
                    plotOptions: {
                        radialBar: {
                            hollow: {
                                size: "70%"
                            }
                        }
                    },
                    colors: colors,
                    series: [percentage],
                    labels: ["Silver"]
                },
                chart = new ApexCharts(document.querySelector("#basic-radialbar"), options);
            chart.render();

    }
    else{
        grade.innerHTML = "Grade : Silver";
        award.innerHTML =`<img src="../img/silver.png" alt="" id="award"class="avatar-title">`;
        percentage1 = (total/50)*100
        percentage = Math.round(percentage1);
        console.log(percentage)
        var colors = ["#CC7000"],
            dataColors = $("#basic-radialbar").data("colors");
            dataColors && (colors = dataColors.split(","));
            var options = {
                    chart: {
                        height: 320,
                        type: "radialBar"
                    },
                    plotOptions: {
                        radialBar: {
                            hollow: {
                                size: "70%"
                            }
                        }
                    },
                    colors: colors,
                    series: [percentage],
                    labels: ["Silver"]
                },
                chart = new ApexCharts(document.querySelector("#basic-radialbar"), options);
            chart.render();

    }
    


}

else{
  grade.innerHTML = "Grade : No grade";
//   award.innerHTML =`<img src="../img/nograde.png" alt="" id="award"class="avatar-title">`;
  percentage1 = (total/35)*100
  percentage = Math.round(percentage1);
  console.log(percentage)
  var colors = ["#CC7000"],
  dataColors = $("#basic-radialbar").data("colors");
  dataColors && (colors = dataColors.split(","));
  var options = {
          chart: {
              height: 320,
              type: "radialBar"
          },
          plotOptions: {
              radialBar: {
                  hollow: {
                      size: "70%"
                  }
              }
          },
          colors: colors,
          series: [percentage],
          labels: ["No grade"]
      },
      chart = new ApexCharts(document.querySelector("#basic-radialbar"), options);
  chart.render();

}
//

// console.log(participation,leadership,achievement,enrichment,community,service,total)
