const express=require("express");
const admin_route= express();
const session = require("express-session");
const config = require("../config/config");
const path = require("path");
const userController = require("../controllers/userController")
const adminController = require("../controllers/adminController");


admin_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));

const auth = require("../middleware/auth");

// admin_route.set('view engine', 'ejs');
// admin_route.set('views','./views/admin');

// const bodyParser = require('body-parser');
// admin_route.use(bodyParser.json());
// admin_route.use(bodyParser.urlencoded({extended:true}))

const multer = require("multer");

admin_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});
//  admin_route.use(express.static(path.resolve(__dirname,'public')));

const excelStorage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname,'../public/excel'));
    },
    filename:function(req,file,cb){
        cb(null, file.originalname);
    }
});
const uploadExcel = multer({storage:excelStorage});



admin_route.post('/register', upload.single('image'),adminController.registerAdmin);

// admin_route.get('/Manageuser',auth.isLogin,adminController.loadManageuser)

admin_route.get('/adminDashboard',auth.isLogin,userController.restrictTo('admin'),adminController.loadDashboard,)

admin_route.get('/staffManage',auth.isLogin,userController.restrictTo('admin'),adminController.loadstaffManage)

admin_route.get('/addIndividual',auth.isLogin,userController.restrictTo('admin'),adminController.loadIndividualRegisteration)
admin_route.post('/addIndividual',adminController.IndividualRegisterationStudent)

// admin_route.get('/ManageStaff',auth.isLogin,adminController.loadManageStaff)

admin_route.get('/addIndividualStaff',auth.isLogin,userController.restrictTo('admin'),adminController.loadIndividualRegisterationStaff)
admin_route.post('/addIndividualStaff',adminController.IndividualRegisterationStaff)

admin_route.post('/studentBulk',uploadExcel.single('csvFileInput'),adminController.studentBulk);
admin_route.get('/studentBulk',auth.isLogin,userController.restrictTo('admin'),adminController.loadStudentBulk)

admin_route.get('/staffBulk',auth.isLogin,userController.restrictTo('admin'),adminController.loadStaffBulk)
admin_route.post('/staffBulk',uploadExcel.single('csvFileInput'),adminController.staffBulk);

// admin_route.get('/searchUser',auth.isLogin,adminController.loadsearch)

admin_route.get('/editStudent',auth.isLogin,userController.restrictTo('admin'),adminController.loadEditStudent);
admin_route.post('/editStudent',adminController.updateStudent);

admin_route.get('/ccaManage',auth.isLogin,userController.restrictTo('admin'),adminController.loadccaManage);

admin_route.get('/setting',auth.isLogin,userController.restrictTo('admin'),adminController.loadsetting);
admin_route.get('/studentProfile',auth.isLogin,userController.restrictTo('admin'),adminController.loadstudentprofile);

admin_route.get('/staffProfile',auth.isLogin,userController.restrictTo('admin'),adminController.loadstaffprofile);

admin_route.get('/ccaIndividual',auth.isLogin,userController.restrictTo('admin'),adminController.loadccaIndividual)
admin_route.post('/ccaIndividual',adminController.ccaIndividual)

admin_route.get('/ccaBulk',auth.isLogin,userController.restrictTo('admin'),adminController.loadccaBulk);
admin_route.post('/ccaBulk',uploadExcel.single('csvFileInput'),adminController.ccaBulk);

admin_route.get('/delete-user',auth.isLogin,userController.restrictTo('admin'),adminController.deleteStudentLoad);
admin_route.get('/delete-staff',auth.isLogin,adminController.deleteStaffLoad);

admin_route.get('/editStaff',auth.isLogin,userController.restrictTo('admin'),adminController.loadEditStaff);
admin_route.post('/editStaff',adminController.updateStaff);
// user_route.get('/userprofile',auth.isLogin,userController.userProfileLoad);

admin_route.get('/editCca',auth.isLogin,adminController.loadEditCca);
admin_route.post('/editCca',auth.isLogin,adminController.updateCca);

admin_route.get('/delete-cca',auth.isLogin,adminController.deleteCca);

// user_route.get('/edit',auth.isLogin,userController.editUserProfileLoad);
admin_route.post('/updateProfile',upload.single('image'),adminController.updateProfile);
admin_route.post('/updatePassword',auth.isLogin,adminController.updatePassword);

// user_route.get('/verification', userController.verificationLoad);
// user_route.post('/verifsication', userController.sentVerificationLink);

admin_route.get('/eventDetails',auth.isLogin,userController.restrictTo('admin'),adminController.loadViewMoreEvent);
admin_route.get('/adminabout',auth.isLogin,userController.restrictTo('admin'),adminController.loadAbout);



module.exports=admin_route;
