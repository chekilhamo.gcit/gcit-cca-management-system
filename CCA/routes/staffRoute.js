const express=require("express");
const staff_route= express();
const session = require("express-session");
const config = require("../config/config");
const auth = require("../middleware/auth");
const path = require("path");

const staffController = require("../controllers/staffController");
const userController = require("../controllers/userController")

staff_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));

const multer = require("multer");

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/excel'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});

const storageimage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const uploadImage = multer({storage:storageimage});

// staff_route.set('view engine', 'ejs');
// staff_route.set('views','./views/staff');

// const bodyParser = require('body-parser');
// staff_route.use(bodyParser.json());
// staff_route.use(bodyParser.urlencoded({extended:true}))

staff_route.get('/staffDashboard',auth.isLogin,userController.restrictTo('staff'),staffController.loadStaffDashboard)

staff_route.get('/UserDetails',auth.isLogin,userController.restrictTo('staff'),staffController.loadUserDetails)

staff_route.get('/viewStudentDetails',auth.isLogin,userController.restrictTo('staff'),staffController.loadViewUserDetails)

staff_route.get('/ccaRequest',auth.isLogin,userController.restrictTo('staff'),staffController.loadccaRequest)
staff_route.post('/ccaRequest',upload.single('cca'),staffController.uploadCSV)


staff_route.get('/eventRequest',auth.isLogin,userController.restrictTo('staff'),staffController.loadeventRequest)
staff_route.post('/eventRequest',upload.single('supportingDocument'),staffController.createEventRequest)

staff_route.get('/viewMore',auth.isLogin,userController.restrictTo('staff'),staffController.loadviewMore)

staff_route.get('/staffSetting',auth.isLogin,userController.restrictTo('staff'),staffController.loadSetting)
// staff_route.post('/staffSetting',uploadImage.single('image'),staffController.updateUserProfile);

staff_route.post('/updateProfileStaff',uploadImage.single('image'),staffController.updateProfile);
staff_route.post('/updatePasswordStaff',auth.isLogin,staffController.updatePassword);
staff_route.get('/staffabout',auth.isLogin,userController.restrictTo('staff'),staffController.loadAbout);


// staff_route.get('/delete-ccafile',auth.isLogin,staffController.deleteccafile);

module.exports=staff_route;

