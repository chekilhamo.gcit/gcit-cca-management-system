const express=require("express");
const student_route= express();
const session = require("express-session");
const config = require("../config/config");
const studentController = require("../controllers/studentController");
const userController = require("../controllers/userController")

const path = require("path");

student_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));

// student_route.set('view engine','ejs');
// student_route.set('views','./views/student');


const auth = require("../middleware/auth");

const multer = require("multer");

student_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});

student_route.get('/studentDashboard',auth.isLogin,userController.restrictTo('student'),studentController.loadStudentDashboard)

student_route.get('/cca',auth.isLogin,userController.restrictTo('student'),studentController.loadcca)

student_route.get('/studentSetting',auth.isLogin,userController.restrictTo('student'),studentController.loadStudentSetting)

student_route.post('/updateProfilestd',upload.single('image'),studentController.updateProfile);
student_route.post('/updatePasswordstd',auth.isLogin,userController.restrictTo('student'),studentController.updatePassword);

student_route.get('/about',auth.isLogin,userController.restrictTo('student'),studentController.loadAbout);


module.exports=student_route;

