const express=require("express");
const user_route= express();
const session = require("express-session");
const config = require("../config/config");
const path = require("path");
const auth = require("../middleware/auth");

const userController = require("../controllers/userController");


user_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));


// user_route.set('view engine', 'ejs');
// user_route.set('views','./views/admin');

// const bodyParser = require('body-parser');
// user_route.use(bodyParser.json());
// user_route.use(bodyParser.urlencoded({extended:true}))

const multer = require("multer");

user_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});



user_route.get('/register',userController.loadRegister);

// user_route.get('/verify', userController.verifyMail);

user_route.get('/', userController.loginLoad);
user_route.get('/login', userController.loginLoad);

user_route.post('/login', userController.verifyLogin);

user_route.get('/logout',auth.isLogin,userController.logout);

user_route.get('/forget', userController.forgetLoad);
user_route.post('/forget', userController.forgetVerify);

user_route.get('/reset-password',userController.resetPasswordLoad);
user_route.post('/reset-password', userController.resetPassword);

user_route.get('/otp', userController.loadOTP);
user_route.post('/otp', userController.enterOTP);
// user_route.get('/verification', userController.verificationLoad);
// user_route.post('/verification', userController.sentVerificationLink);

module.exports=user_route;
