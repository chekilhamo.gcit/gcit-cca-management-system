const express=require("express");
const hos_route= express();
const session = require("express-session");
const config = require("../config/config");
const hosController = require("../controllers/hosController");
const userController = require("../controllers/userController")
const path = require("path");

hos_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));

const auth = require("../middleware/auth");

const multer = require("multer");

hos_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});

hos_route.get('/hosDashboard',auth.isLogin,userController.restrictTo('hos'),hosController.loadHosDashboard)

hos_route.get('/studentlist',auth.isLogin,userController.restrictTo('hos'),hosController.loadStudentList)

hos_route.get('/ccarequestList',auth.isLogin,userController.restrictTo('hos'),hosController.loadccarequestList)

hos_route.get('/hosSetting',auth.isLogin,userController.restrictTo('hos'),hosController.loadsetting);

// hos_route.post('/hosSetting',upload.single('image'),hosController.updateUserProfile);

hos_route.get('/hosViewMoreEvent',auth.isLogin,userController.restrictTo('hos'),hosController.loadViewMoreEvent);

hos_route.get('/hosViewStudentProfile',auth.isLogin,userController.restrictTo('hos'),hosController.loadStudentProfile);

// hos_route.post('/hosViewMoreEvent/approve', auth.isLogin, hosController.statusUpdate);
// hos_route.post('/hosViewMoreEvent/reject', auth.isLogin, hosController.statusUpdate);

hos_route.post("/approve-event", auth.isLogin, hosController.approveEvent);
hos_route.post("/reject-event", auth.isLogin, hosController.rejectEvent);
hos_route.get('/viewccarequest',auth.isLogin,userController.restrictTo('hos'),hosController.loadViewccarequest);

hos_route.post("/approve-cca", auth.isLogin, hosController.approveCCA);
hos_route.post("/reject-cca", auth.isLogin, hosController.rejectCCA);


hos_route.post('/updateProfilehos',upload.single('image'),hosController.updateProfile);
hos_route.post('/updatePasswordhos',auth.isLogin,hosController.updatePassword);
hos_route.get('/hosabout',auth.isLogin,userController.restrictTo('hos'),hosController.loadAbout);


module.exports=hos_route;