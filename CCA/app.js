const express = require("express")
const path = require('path')
const bodyParser = require('body-parser');
const session = require('express-session');
const app = express()
const xss = require('xss-clean')
const { startSchedule } =require('./models/backup')

app.use(xss())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}))
app.set("view engine", "ejs");

//for admin routes
const userRoute=require('./routes/userRoutes');
const adminRoute=require('./routes/adminRoute');
const studentRoute=require('./routes/studentRoute');
const staffRoute=require('./routes/staffRoute');
const hosRoute = require("./routes/hosRoute");

startSchedule()

// app.use('/',viewRoute);
app.use('/',userRoute);
app.use('/',adminRoute);
app.use('/',studentRoute);
app.use('/',staffRoute);
app.use('/',hosRoute);

app.use(express.static(path.join(__dirname, "views")));
module.exports = app